﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace GraphicsAssignmentThree
{
    class World
    {
        GraphicsDeviceManager graphics;
        IServiceProvider game1Service;

        // Data structures required to store earth\terrain
        Model world;
        Matrix[] world_boneTransforms;
        Matrix world_transform;
             
        public World()
        {
        }

        public World(GraphicsDeviceManager graphics, IServiceProvider game1Service)
        {
            this.graphics = graphics;
            this.game1Service = game1Service;
        }

        public void LoadWorld()
        {
            ContentManager contentLoader = new ContentManager(game1Service);

            // Load model from run-time directory (debug/release)
            world = contentLoader.Load<Model>(@"Content\Models\World\houses");
            world_boneTransforms = new Matrix[world.Bones.Count];
            world.CopyAbsoluteBoneTransformsTo(world_boneTransforms);
            world_transform = world.Root.Transform * Matrix.CreateScale(0.01f); // 0.05 Campus

        }

        public void DrawWorld(GameTime gameTime, Matrix view, Matrix projection)
        {
           // GraphicsDevice.Clear(Color.Black);

            // Draw world
            world.CopyAbsoluteBoneTransformsTo(world_boneTransforms);
            world.Root.Transform = world_transform;
            foreach (ModelMesh mesh in world.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.AmbientLightColor = new Vector3(1.0f, 1.0f, 1.0f);
                    effect.EmissiveColor = new Vector3(1, 1, 1);
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = world_boneTransforms[mesh.ParentBone.Index];
                }
                mesh.Draw();
            }
        }

        public void UpdateWorld(KeyboardState keys, Matrix view, Matrix projection)
        {
            // Rotate model (static at moment)
            world_transform *= Matrix.CreateRotationY(0.000f);

        }
    }
}
