using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace GraphicsAssignmentThree
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        // The GraphicsDeviceManger looks after graphical hardware i.e. The graphics card.
        // The class interacts with all the graphics hardware. Example: setting game window size. 
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        // Camera 
        Matrix view, projection;
        //float theta = 0.0f;
        PrimitiveSkyBox pSkyBox;
        SkyBox skybox;
        World myWorld;
        Tiny tiny;
        Motorbike bike;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            pSkyBox = new PrimitiveSkyBox(graphics, this.Services);
            skybox = new SkyBox(graphics, this.Services);
            myWorld = new World(graphics, this.Services);
            tiny = new Tiny(graphics, this.Services);
            bike = new Motorbike(graphics, this.Services);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            pSkyBox.LoadSkyBox();
            skybox.LoadSkyBox();
            myWorld.LoadWorld();
            tiny.LoadTiny();
            tiny.ptinyRotation = Quaternion.Identity;
            bike.LoadMotorbike();
            bike.pbikeRotation = Quaternion.Identity;
           
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            KeyboardState keys = Keyboard.GetState();
            if (keys.IsKeyDown(Keys.Q)) { this.Exit(); }
            myWorld.UpdateWorld(keys, view, projection);



            //  Camera relative to Tiny
            Vector3 campos = Vector3.Transform(new Vector3(0, 1.5f, -3.5f), Matrix.CreateFromQuaternion(tiny.ptinyRotation));
            // Movement of the camera with Tiny. With a + it follows her, with a - it move towards her then away.
            //campos += tiny.ptinyPosition;

            Vector3 camup = new Vector3(0, 1, 0);
            camup = Vector3.Transform(camup, Matrix.CreateFromQuaternion(tiny.ptinyRotation));

            view = Matrix.CreateLookAt(campos, tiny.ptinyPosition, camup);
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, graphics.GraphicsDevice.Viewport.AspectRatio, 0.2f, 500.0f);

            #region // Tiny update

            Vector3 changepos = Vector3.Transform(Vector3.Forward, Matrix.CreateFromQuaternion(tiny.ptinyRotation));
            //tiny.ptinyPosition -= Vector3.Multiply(changepos, 0.05f);

            if (keys.IsKeyDown(Keys.Space))
            {
                tiny.ptinyPosition = new Vector3(0, 2, -24);
                tiny.ptinyRotation = Quaternion.Identity;
            }

            if (keys.IsKeyDown(Keys.W))
            {
                campos += tiny.ptinyPosition;
                tiny.ptinyPosition -= Vector3.Multiply(changepos, 0.05f);
            }

            // Reset yaw, pitch and roll each time
            tiny.pdx = 0f; tiny.pdy = 0f; tiny.pdz = 0f;
            // Yaw
            if (keys.IsKeyDown(Keys.Right)) tiny.pdy = -0.01f;
            if (keys.IsKeyDown(Keys.Left)) tiny.pdy = 0.01f;
            // Pitch
            if (keys.IsKeyDown(Keys.Down)) tiny.pdx = -0.01f;
            if (keys.IsKeyDown(Keys.Up)) tiny.pdx = 0.01f;
            // Roll 
            if (keys.IsKeyDown(Keys.OemComma)) tiny.pdz = 0.01f;
            if (keys.IsKeyDown(Keys.OemPeriod)) tiny.pdz = -0.01f;

            tiny.ptinyRotation *= Quaternion.CreateFromAxisAngle(Vector3.Up, tiny.pdy) * Quaternion.CreateFromAxisAngle(Vector3.Right, tiny.pdx) * Quaternion.CreateFromAxisAngle(Vector3.Backward, tiny.pdz);
            #endregion

            base.Update(gameTime); 
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            pSkyBox.DrawSkyBox(gameTime, view, projection);
            skybox.DrawSkyBox(gameTime, view, projection);
            myWorld.DrawWorld(gameTime, view, projection);
            tiny.DrawTiny(gameTime, view, projection);
            bike.DrawMotorbike(gameTime, view, projection);

            base.Draw(gameTime);
        }
    }
}
