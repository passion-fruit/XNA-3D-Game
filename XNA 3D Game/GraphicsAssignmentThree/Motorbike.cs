﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace GraphicsAssignmentThree
{

    class Motorbike
    {
        GraphicsDeviceManager graphics;
        IServiceProvider game1Service;


        // Data structures required to store Tiny
        Model bike;
        Matrix[] bike_boneTransforms;
        Matrix bike_transform;

        // Tiny position (Camera located above and behind Tiny)
        Vector3 bikePosition = new Vector3(2, 2, 0);
        Quaternion bikeRotation = Quaternion.Identity;
        float dx, dy, dz;


        public Motorbike(GraphicsDeviceManager graphics, IServiceProvider game1Service)
        {
            this.graphics = graphics;
            this.game1Service = game1Service;
        }

        public void LoadMotorbike()
        {
            ContentManager contentLoader = new ContentManager(game1Service);
            // Load model from run-time directory (debug/release)
            bike = contentLoader.Load<Model>(@"Content\Models\Motorbike\moto2");
            bike_boneTransforms = new Matrix[bike.Bones.Count];
            bike.CopyAbsoluteBoneTransformsTo(bike_boneTransforms);
            bike_transform = bike.Root.Transform * Matrix.CreateScale(0.001f); // 0.05 Campus

            //bike_transform = bike.Root.Transform * Matrix.CreateScale(0.003f) * Matrix.CreateRotationX(MathHelper.PiOver2) * Matrix.CreateRotationY(MathHelper.Pi);
        }

        public void DrawMotorbike(GameTime gameTime, Matrix view, Matrix projection)
        {
            bike.CopyAbsoluteBoneTransformsTo(bike_boneTransforms);
            // Resets the position of Tiny to her original position evey time a frame is drawn.
            // tiny.Root.Transform = tiny_transform;

            // Draws Tiny at her new location in every frame.
            bike.Root.Transform = bike_transform * Matrix.CreateFromQuaternion(bikeRotation) * Matrix.CreateTranslation(bikePosition);

            foreach (ModelMesh mesh in bike.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = bike_boneTransforms[mesh.ParentBone.Index];
                }
                mesh.Draw();
            }


        }

        public void UpdateTiny(KeyboardState keys, Matrix view, Matrix projection)
        {

        }

        #region /* Getter and Setter methods */
        public Vector3 pbikePosition { get { return bikePosition; } set { bikePosition = value; } }
        public Quaternion pbikeRotation { get { return bikeRotation; } set { bikeRotation = value; } }
        public float pdx { get { return dx; } set { dx = value; } }
        public float pdy { get { return dy; } set { dy = value; } }
        public float pdz { get { return dz; } set { dz = value; } }
        #endregion




    }
}
