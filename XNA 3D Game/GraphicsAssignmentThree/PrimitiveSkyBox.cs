﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GraphicsAssignmentThree
{
    class PrimitiveSkyBox
    {
        GraphicsDeviceManager graphics;
        BasicEffect effect;
        IServiceProvider game1Service;

        // Cube and skybox vertices
        VertexPositionTexture[] cubeVertices;
        VertexPositionTexture[] TempList;
        VertexPositionTexture[,] skyboxVertices;

        Texture2D texture;
        Texture2D[] sky_textures = new Texture2D[6];
        VertexDeclaration texturedVertexDeclaration;

        // Various objects need to be passed into this class to creat the skybox  in Game1.cs
        public PrimitiveSkyBox(GraphicsDeviceManager graphics, IServiceProvider game1Service)
        {
            this.graphics = graphics;
            this.game1Service = game1Service;
        }

        public void LoadSkyBox()
        {
            ContentManager contentLoader = new ContentManager(game1Service);
            effect = new BasicEffect(this.graphics.GraphicsDevice, null);
            effect.TextureEnabled = true;

            cubeVertices = new VertexPositionTexture[36];
            cubeVertices[0] = new VertexPositionTexture(new Vector3(-2, 2, 2), new Vector2(0, 0));
            cubeVertices[1] = new VertexPositionTexture(new Vector3(2, -2, 2), new Vector2(0.166f, 1));
            cubeVertices[2] = new VertexPositionTexture(new Vector3(-2, -2, 2), new Vector2(0, 1));
            cubeVertices[3] = new VertexPositionTexture(new Vector3(-2, 2, 2), new Vector2(0, 0));
            cubeVertices[4] = new VertexPositionTexture(new Vector3(2, 2, 2), new Vector2(0.166f, 0));
            cubeVertices[5] = new VertexPositionTexture(new Vector3(2, -2, 2), new Vector2(0.166f, 1));

            cubeVertices[6] = new VertexPositionTexture(new Vector3(2, -2, -2), new Vector2(0.167f, 1));
            cubeVertices[7] = new VertexPositionTexture(new Vector3(2, 2, -2), new Vector2(0.167f, 0));
            cubeVertices[8] = new VertexPositionTexture(new Vector3(-2, 2, -2), new Vector2(0.333f, 0));
            cubeVertices[9] = new VertexPositionTexture(new Vector3(-2, -2, -2), new Vector2(0.333f, 1));
            cubeVertices[10] = new VertexPositionTexture(new Vector3(2, -2, -2), new Vector2(0.167f, 1));
            cubeVertices[11] = new VertexPositionTexture(new Vector3(-2, 2, -2), new Vector2(0.333f, 0));

            cubeVertices[12] = new VertexPositionTexture(new Vector3(2, -2, 2), new Vector2(0.334f, 1));
            cubeVertices[13] = new VertexPositionTexture(new Vector3(2, 2, -2), new Vector2(0.5f, 0));
            cubeVertices[14] = new VertexPositionTexture(new Vector3(2, -2, -2), new Vector2(0.5f, 1));
            cubeVertices[15] = new VertexPositionTexture(new Vector3(2, -2, 2), new Vector2(0.334f, 1));
            cubeVertices[16] = new VertexPositionTexture(new Vector3(2, 2, 2), new Vector2(0.334f, 0));
            cubeVertices[17] = new VertexPositionTexture(new Vector3(2, 2, -2), new Vector2(0.5f, 0));

            cubeVertices[18] = new VertexPositionTexture(new Vector3(-2, 2, -2), new Vector2(0.5f, 0));
            cubeVertices[19] = new VertexPositionTexture(new Vector3(-2, -2, 2), new Vector2(0.666f, 1));
            cubeVertices[20] = new VertexPositionTexture(new Vector3(-2, -2, -2), new Vector2(0.5f, 1));
            cubeVertices[21] = new VertexPositionTexture(new Vector3(-2, 2, -2), new Vector2(0.5f, 0));
            cubeVertices[22] = new VertexPositionTexture(new Vector3(-2, 2, 2), new Vector2(0.666f, 0));
            cubeVertices[23] = new VertexPositionTexture(new Vector3(-2, -2, 2), new Vector2(0.666f, 1));

            cubeVertices[24] = new VertexPositionTexture(new Vector3(-2, -2, 2), new Vector2(0.667f, 0));
            cubeVertices[25] = new VertexPositionTexture(new Vector3(2, -2, -2), new Vector2(0.833f, 1));
            cubeVertices[26] = new VertexPositionTexture(new Vector3(-2, -2, -2), new Vector2(0.667f, 1));
            cubeVertices[27] = new VertexPositionTexture(new Vector3(-2, -2, 2), new Vector2(0.667f, 0));
            cubeVertices[28] = new VertexPositionTexture(new Vector3(2, -2, 2), new Vector2(0.833f, 0));
            cubeVertices[29] = new VertexPositionTexture(new Vector3(2, -2, -2), new Vector2(0.833f, 1));

            cubeVertices[30] = new VertexPositionTexture(new Vector3(2, 2, -2), new Vector2(0.833f, 0));
            cubeVertices[31] = new VertexPositionTexture(new Vector3(-2, 2, 2), new Vector2(1.0f, 1));
            cubeVertices[32] = new VertexPositionTexture(new Vector3(-2, 2, -2), new Vector2(0.833f, 1));
            cubeVertices[33] = new VertexPositionTexture(new Vector3(2, 2, -2), new Vector2(0.833f, 0));
            cubeVertices[34] = new VertexPositionTexture(new Vector3(2, 2, 2), new Vector2(1.0f, 0));
            cubeVertices[35] = new VertexPositionTexture(new Vector3(-2, 2, 2), new Vector2(1.0f, 1));

            // The skybox, (inside the cube)
            // 6 Sides each created using 2 triangles each triangle has 3 verices = 36 vertices.
            skyboxVertices = new VertexPositionTexture[6, 6];

            sky_textures[0] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_front");
            sky_textures[1] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_bottom");
            sky_textures[2] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_right");
            sky_textures[3] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_left");
            sky_textures[4] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_bottom");
            sky_textures[5] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_top");

            // Front
            skyboxVertices[0, 0] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(0, 1));
            skyboxVertices[0, 1] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(1, 1));
            skyboxVertices[0, 2] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(0, 0));
            skyboxVertices[0, 3] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(1, 0));
            skyboxVertices[0, 4] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(0, 0));
            skyboxVertices[0, 5] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(1, 1));

            // Back
            skyboxVertices[1, 0] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(0, 1));
            skyboxVertices[1, 1] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(1, 1));
            skyboxVertices[1, 2] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(1, 0));
            skyboxVertices[1, 3] = new VertexPositionTexture(new Vector3(-50, 50, -50), new Vector2(0, 0));
            skyboxVertices[1, 4] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(0, 1));
            skyboxVertices[1, 5] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(1, 0));

            // Right
            skyboxVertices[2, 0] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(1, 1));
            skyboxVertices[2, 1] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 0));
            skyboxVertices[2, 2] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(0, 1));
            skyboxVertices[2, 3] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(1, 1));
            skyboxVertices[2, 4] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(1, 0));
            skyboxVertices[2, 5] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 0));

            // Left
            skyboxVertices[3, 0] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(0, 0));
            skyboxVertices[3, 1] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(1, 1));
            skyboxVertices[3, 2] = new VertexPositionTexture(new Vector3(-50, 50, -50), new Vector2(1, 0));
            skyboxVertices[3, 3] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(0, 0));
            skyboxVertices[3, 4] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(0, 1));
            skyboxVertices[3, 5] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(1, 1));

            // Ground
            skyboxVertices[4, 0] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(0, 1));
            skyboxVertices[4, 1] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(1, 0));
            skyboxVertices[4, 2] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(0, 0));
            skyboxVertices[4, 3] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(0, 1));
            skyboxVertices[4, 4] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(1, 1));
            skyboxVertices[4, 5] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(1, 0));

            // Sky
            skyboxVertices[5, 0] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(1, 0));
            skyboxVertices[5, 1] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(1, 1));
            skyboxVertices[5, 2] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 1));
            skyboxVertices[5, 3] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(1, 0));
            skyboxVertices[5, 4] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 1));
            skyboxVertices[5, 5] = new VertexPositionTexture(new Vector3(-50, 50, -50), new Vector2(0, 0));

            sky_textures[0] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_front");
            sky_textures[1] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_bottom");
            sky_textures[2] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_right");
            sky_textures[3] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_left");
            sky_textures[4] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_bottom");
            sky_textures[5] = contentLoader.Load<Texture2D>(@"Content\Models\SkyBox\skybox_top");

            // Front
            skyboxVertices[0, 0] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(0, 1));
            skyboxVertices[0, 1] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(0, 0));
            skyboxVertices[0, 2] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(1, 1));
            skyboxVertices[0, 3] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(1, 0));
            skyboxVertices[0, 4] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(1, 1));
            skyboxVertices[0, 5] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(0, 0));


            // Back
            skyboxVertices[1, 0] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(0, 1));
            skyboxVertices[1, 1] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(1, 0));
            skyboxVertices[1, 2] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(1, 1));
            skyboxVertices[1, 3] = new VertexPositionTexture(new Vector3(-50, 50, -50), new Vector2(0, 0));
            skyboxVertices[1, 4] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(1, 0));
            skyboxVertices[1, 5] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(0, 1));


            // Right
            skyboxVertices[2, 0] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(1, 1));
            skyboxVertices[2, 1] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(0, 1));
            skyboxVertices[2, 2] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 0));
            skyboxVertices[2, 3] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(1, 1));
            skyboxVertices[2, 4] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 0));
            skyboxVertices[2, 5] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(1, 0));

            // Left
            skyboxVertices[3, 0] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(0, 0));
            skyboxVertices[3, 1] = new VertexPositionTexture(new Vector3(-50, 50, -50), new Vector2(1, 0));
            skyboxVertices[3, 2] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(1, 1));
            skyboxVertices[3, 3] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(0, 0));
            skyboxVertices[3, 4] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(1, 1));
            skyboxVertices[3, 5] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(0, 1));

            // Ground
            skyboxVertices[4, 0] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(1, 0));
            skyboxVertices[4, 1] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(0, 1));
            skyboxVertices[4, 2] = new VertexPositionTexture(new Vector3(-50, -50, -50), new Vector2(0, 0));
            skyboxVertices[4, 3] = new VertexPositionTexture(new Vector3(-50, -50, 50), new Vector2(0, 1));
            skyboxVertices[4, 4] = new VertexPositionTexture(new Vector3(50, -50, -50), new Vector2(1, 0));
            skyboxVertices[4, 5] = new VertexPositionTexture(new Vector3(50, -50, 50), new Vector2(1, 1));


            // Sky
            skyboxVertices[5, 0] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(1, 0));
            skyboxVertices[5, 1] = new VertexPositionTexture(new Vector3(50, 50, 50), new Vector2(1, 1));
            skyboxVertices[5, 2] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 1));
            skyboxVertices[5, 3] = new VertexPositionTexture(new Vector3(-50, 50, 50), new Vector2(1, 0));
            skyboxVertices[5, 4] = new VertexPositionTexture(new Vector3(50, 50, -50), new Vector2(0, 1));
            skyboxVertices[5, 5] = new VertexPositionTexture(new Vector3(-50, 50, -50), new Vector2(0, 0));

            texturedVertexDeclaration = new VertexDeclaration(this.graphics.GraphicsDevice, VertexPositionTexture.VertexElements);

            this.graphics.GraphicsDevice.VertexDeclaration = new VertexDeclaration(this.graphics.GraphicsDevice, VertexPositionTexture.VertexElements);
        }

        public void DrawSkyBox(GameTime gameTime, Matrix view, Matrix projection)
        {
            TempList = new VertexPositionTexture[6];
            for (int side = 0; side < 6; side++)
            {
                TempList[0] = skyboxVertices[side, 0];
                TempList[1] = skyboxVertices[side, 1];
                TempList[2] = skyboxVertices[side, 2];
                TempList[3] = skyboxVertices[side, 3];
                TempList[4] = skyboxVertices[side, 4];
                TempList[5] = skyboxVertices[side, 5];

                effect.Begin();
                effect.Texture = sky_textures[side];
                foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                {
                    pass.Begin();
                    this.graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, TempList, 0, 2);
                    pass.End();
                }
                effect.End();
            }

        }
    }
}